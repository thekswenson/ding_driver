# Glue DING Together #

Install Dependencies:

- snakemake

- CPLEX: make link to the executable in the top of this repository _e.g._ `ln -s /path/to/cplex`.

- [DING](https://gitlab.ub.uni-bielefeld.de/gi/ding): make a link to the ding install directory in the top of this repo _e.g._ `ln -s /path/to/dingdir`.

- [UniMog](https://bibiserv.cebitec.uni-bielefeld.de/dcj?id=dcj_view_download): put the .jar in the the ding directory

My `ding_driver` directory looks like this after installation:
```
cplex@  ding@  examples/  readme.md  Snakefile
```

## Input File Format ##

The format is [UniMoG format](https://bibiserv.cebitec.uni-bielefeld.de/dcj?id=dcj_manual).

## Running the DING ##

Use snakemake to run the many steps of DING. Specify the input file using the `--config infile=<inputfile>.fasta` syntax.
If you want to specify an extra output directory to contain the results you can use the `--config outdir=<thedir>/` syntax.
For example you can run `snakemake -j1 --config infile=examples/groupindel1.fasta outdir=output/` to test the installation.
