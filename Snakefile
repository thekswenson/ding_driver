# coding: utf-8
"""
Run this from the commandline specifying the input unimog file and output
directory prefix with
  `snakemake --config infile=examples/test4.fasta outdir=output`.
"""
import sys
import csv

from collections import defaultdict
from itertools import combinations

#configfile: 'snakeconfig/default.yaml'

#### BASIC CONSTANTS ####

execdir = 'ding/'

#### CONFIG VALUES ####

if 'infile' in config:
  unimogfile = config['infile']
  dataset = Path(unimogfile).stem
else:
  sys.exit('No unimog file specified. (please use "--config infile=<name>.fasta"')

dirprefix = config['outdir'] if 'outdir' in config else ''
if dirprefix and dirprefix[-1] != '/': dirprefix += '/'


#### SPECIFING TARGETS ####

def buildTargetList(wildcards):
  """
  Build the list of ultimate targets based on the current set of constants.
  """
  retlist = getScenarioFilenames()

  #if len(retlist) > 1:    #distance matrix
  retlist.append(f'{dirprefix}{dataset}_output/{dataset}_matrix.csv')

  return retlist

def getScenarioFilenames():
  """
  Return the list of scenario filenames based on genomes in the unimog file.
  """
  return getFilenames('scenario')

def getDistanceFilenames(wildcards=None):
  """
  Return the list of distance filenames based on genomes in the unimog file.
  """
  return getFilenames('distance')

def getFilenames(suffix):
  """
  Return the list of filenames based on genomes in the unimog file. `suffix`
  should be the string indicating the filetype to get.
  """
  names = getSeqNames(unimogfile)
  return [f'{dirprefix}{dataset}_output/{dataset}__{n}__{m}_{suffix}.txt'
          for n, m in combinations(names, 2)]
  
def getSeqNames(filename):
  """
  Return a list of sequence names from the given unimog filename.
  """
  names = []
  with open(filename) as f:
    for line in f:
      line = line.strip()
      if line[0] == '>':
        tokens = line[1:].split()

        if len(tokens) > 1:
          print(f'WARN: name with spaces found: "{line}".', file=sys.stderr)

        if len(tokens) == 1:
          if '__' in tokens[0]:
            sys.exit(f'Forbidden characters "__" found in line "{line}".')
          names.append(tokens[0])

        else:
          print(f'Empty name line found in "{filename}"!', file=sys.stderr)

  return names

rule all:
  input:
    buildTargetList,

#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-


rule get_scenario:
  input:
    '{outdir}/{dataset}_relabeled.txt',

  output:
    '{outdir}/{dataset}_scenario.txt',

  shell:
    'java -jar {execdir}UniMoG-java11.jar -m=6 {input} > {output}'

rule get_distance:
  input:
    idfile = '{outdir}/{dataset}_id.txt',
    solution = '{outdir}/{dataset}.sol',

  output:
    distance = '{outdir}/{dataset}_distance.txt',
    relabeled = '{outdir}/{dataset}_relabeled.txt',

  shell:
    '{execdir}parse_cplex_sol.py -i {input.solution} -u {input.idfile} -o {output.relabeled} > {output.distance}'

rule run_cplex:
  input:
    '{outdir}/{dataset}_lp.txt',

  output:
    '{outdir}/{dataset}.sol',

  log:
    '{outdir}/{dataset}_lp.log',

  shell:
    'cplex -c "set logfile {log}" "read {input} lp" "mipopt" "write {output}"  > /dev/null'

rule create_ilp:
  input:
    unimogfile,

  output:
    idfile = '{outdir}/{datasetbase}__{n}__{m}_id.txt',
    lp = '{outdir}/{datasetbase}__{n}__{m}_lp.txt',

  log:
    '{outdir}/{datasetbase}__{n}__{m}.log'

  shell:
    '{execdir}unimog_to_ilp.py -1 {wildcards.n} -2 {wildcards.m} -i {input} -u {output.idfile} -o {output.lp} -l {log}'


distfilere = re.compile(r'\S+_output/\S+__(\S+)__(\S+)_distance.txt')
def genomesFromDistFilename(filename):
  """
  Return the two genomes encoded in the filename
  """
  m = distfilere.match(filename)
  if m:
    return m.group(1), m.group(2)
  else:
    sys.exit(f'ERROR: unexpected distance filename "{filename}".')

distre = re.compile(r'integer optimal solution\s+(\d+)\s+(\d+)')
rule get_matrix:
  input:
    getDistanceFilenames,

  output:
    '{dirprefix}{dataset}_output/{dataset}_matrix.csv'

  run:
    genomeTOgenomeTOdist = defaultdict(dict)
    for distfile in input:
      with open(distfile) as f:
        f.readline()
        line = f.readline()
        match = distre.match(line)
        if match:
          n, m = genomesFromDistFilename(distfile)

          genomeTOgenomeTOdist[n][m] = match.group(2)
          genomeTOgenomeTOdist[m][n] = match.group(2)
        else:
          print(f'WARN: unexpected distance line in "{distfile}".', file=sys.stderr)

    with open(str(output), 'w', newline='') as csvfile:
      genomes = sorted(genomeTOgenomeTOdist.keys())

      writer = csv.DictWriter(csvfile, fieldnames=genomes)
      writer.writeheader()
      for genome in genomes:
        genomeTOgenomeTOdist[genome][genome] = 0
        writer.writerow(genomeTOgenomeTOdist[genome])